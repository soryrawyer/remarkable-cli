# remarkable-cli
an unremarkable cli for the [remarkable 2 tablet](https://remarkable.com/)

## installation
right now, just cloning the repo. this project uses Python 3.8 with [pipenv](https://github.com/pypa/pipenv) for dependency management. once you have those installed, run `pipenv install` to install all dependencies for this tool.

### development
this project uses `black` and `mypy`, which can be installed via `pipenv install --dev`

## usage
### authentication
to authenticate, you will need to get a one-time code. while signed into your remarkable account, you can get an auth code from this URL: https://my.remarkable.com/connect/mobile

this only needs to be *run one time per device*

```shell
$ python remark.py auth "abcdefgh"
```

### upload
```shell
# uploading documents will go to the "root" folder by default:
$ python remark.py upload ~/Documents/mars_attacks_the_novelization.pdf

# you can also specify a folder using the --folder flag:
$ python remark.py upload --folder "Papers" ~/Documents/mars_attacks_the_novelization.pdf
```

### list
the `list` sub-command takes a folder name as input and will print every item in that folder

note: while the `-r` option exists, that is not currently functional
```shell
$ python remark.py list "Papers"
```