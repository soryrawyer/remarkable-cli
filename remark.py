"""
command-line tool for the reMarkable 2

structure:
each sub-command has its own class with `add_subparser` and `run` methods
the map COMMANDS maps the text of the sub-command (i.e. - what the user
will enter as the first argument) to the class

when the program is run, each sub-command will add a subparser with their
specific arguments
`main` will call the `run` method of whichever sub-command is passed
"""

import argparse
import os

from dataclasses import dataclass
from functools import partial
from typing import Optional

from rmapy.api import Client
from rmapy.document import ZipDocument
from rmapy.folder import Folder


@dataclass
class Authenticate:
    """
    authenticate the CLI with remarkable's cloud
    """

    @staticmethod
    def add_subparser(subparsers):
        parser = subparsers.add_parser(
            "auth", help="authenticate with the remarkable cloud"
        )
        parser.add_argument("token", type=str)
        return parser

    @staticmethod
    def run(client: Client, args):
        client.register_device(args.token)
        client.renew_token()


@dataclass
class ListFolder:
    """
    list the items in a given folder
    note: this does not currently support recursion
    """

    @staticmethod
    def add_subparser(subparsers):
        parser = subparsers.add_parser("list", help="list contents of a folder")
        parser.add_argument("folder", type=str)
        parser.add_argument("-r", "--recursive", help="list items recursively")
        return parser

    @staticmethod
    def run(client: Client, args):
        folder = get_folder(client, args.folder)
        if folder is None:
            raise Exception("folder not found: {}".format(args.folder))

        for item in client.get_meta_items():
            if item.Parent == folder.ID:
                print(item.VissibleName)


@dataclass
class Upload:
    @staticmethod
    def add_subparser(subparsers):
        parser = subparsers.add_parser("upload", help="upload a file")
        parser.add_argument("path", type=partial(_path, parser))
        parser.add_argument("--folder", default=None)
        return parser

    @staticmethod
    def run(client: Client, args):
        upload_file(client, args.path, args.folder)


def upload_file(client: Client, path: str, folder: Optional[str]):
    """
    upload the file at `path`
    """
    client.renew_token()
    r_folder = Folder(ID="")  # default folder used by rmapy when uploading
    if folder is not None:
        folder_opt = get_folder(client, folder)
        if folder_opt is None:
            raise Exception("folder not found: {}".format(folder))

        r_folder = folder_opt
    doc = ZipDocument(doc=path)
    client.upload(doc, to=r_folder)


def get_folder(client: Client, folder: str) -> Optional[Folder]:
    """
    return a Folder object for the given name, if it exists
    """
    items = client.get_meta_items()
    folders = [i for i in items if isinstance(i, Folder) and i.VissibleName == folder]
    if not folders:
        return None

    return folders[0]


COMMANDS = {
    "auth": Authenticate,
    "list": ListFolder,
    "upload": Upload,
}


def _path(parser: argparse.ArgumentParser, path_str) -> str:
    """
    accept a string of a path (directory or file) and
    return that string if it exists, or raise an exception
    if it doesn't
    """
    if not os.path.exists(path_str):
        parser.print_usage()
        raise ValueError("<path> must exist")
    return path_str


def _parse_args():
    """
    parse command-line arguments
    """
    parser = argparse.ArgumentParser(__doc__)
    levels = ("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL")
    parser.add_argument("--log-level", choices=levels)
    subparsers = parser.add_subparsers(dest="command")
    for command in COMMANDS.values():
        command.add_subparser(subparsers)
    return parser.parse_args()


def main():
    """
    initialize a client and call whichever command was passed
    """
    args = _parse_args()

    client = Client()
    COMMANDS[args.command].run(client, args)


if __name__ == "__main__":
    main()
